/**
 * Enum for specification of supported clients.
 */
public enum ClientTypeEnum {
    SME( "SME", "CTIFIN"),
    PRAGUEFIN("PRAGUEFIN", "PRAGUEFIN"),
    FT("FT", "CITFINFT"),
    RETAIL("RETAIL", "CITRET");

    private final String afterPrefix = "_AF_BATCH";
    private final String clientCode;
    private final String clientPrefix;

    ClientTypeEnum(String clientCode, String clientPrefix){
        this.clientCode = clientCode;
        this.clientPrefix = clientPrefix;
    }

    public String getClientCode() {
        return clientCode;
    }

    public String getClientPrefix() {
        return clientPrefix;
    }

    public String getAfterPrefix() {
        return afterPrefix;
    }

    public String getFileName(){
        return getClientPrefix() + getAfterPrefix();
    }

    @Override
    public String toString() {
        return getClientCode();
    }
}
