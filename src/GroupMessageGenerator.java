import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class generate processing file for input file.
 * There are methods which generate processing file name and copy source file into target folder.
 */
public class GroupMessageGenerator {
    public static final int INPUT_FILE_NAME_LENGTH_LIMIT = 20;
    public static final int SUBJECT_LENGTH_LIMIT = 255;
    public static final int MESSAGE_BODY_LENGTH_LIMIT = 8000;

    /**
     * Generate processing file and create copy of sending file (sourceFile) into selected location (storageLocation)
     * @param order number 1, 2, ...
     * @param clientIndication
     * @param subject subject of message which can have length max to SUBJECT_LENGTH_LIMIT
     * @param messageBody body of message which can have length max to MESSAGE_BODY_LENGTH_LIMIT
     * @param storageLocation location where will be created processing file and copy od source file
     * @param sourceFile file for which will be create processing file
     * @return GroupMessage which have processing file and copy of source file
     */
    public static GroupMessage generateProcessingFile(int order, String clientIndication, String subject, String messageBody, String storageLocation, File sourceFile) {
        ClientTypeEnum clientType = parseClientType(clientIndication);
        validateInputFile(sourceFile, storageLocation);
        validateProcessingFileName(order, clientType, storageLocation);
        validateMessage(subject, messageBody);

        File processingFile = createProcessingFile(storageLocation, parseProcessingFileName(order, clientType), sourceFile.getName(), subject, messageBody);
        File outputFile = createOutputFile(sourceFile, storageLocation);
        return new GroupMessage(processingFile, outputFile);
    }

    /**
     * Parse processing file name from input
     * @param order number 1, 2, ...
     * @param clientType client indication
     * @return
     */
    public static String parseProcessingFileName(int order, ClientTypeEnum clientType) {
        return clientType.getFileName() + "." + String.format("%03d", order);
    }

    /**
     * Parse file name for copy of source file
     * @param sourceFile
     * @param storageLocation
     * @return
     */
    public static String parseOutputFileName(File sourceFile, String storageLocation){
        return storageLocation + "\\" + sourceFile.getName();
    }

    /**
     * Parce clientTypeEnum from input clientIndication code
     * @param clientCode
     * @return
     */
    public static ClientTypeEnum parseClientType(String clientCode) {
        List<ClientTypeEnum> clientTypes = Arrays.stream(ClientTypeEnum.values())
                                            .filter(clientType -> clientCode.equals(clientType.getClientCode()))
                                            .collect(Collectors.toList());
        if(clientTypes.isEmpty()){
            throw new IllegalArgumentException("Client mark \"" + clientCode + "\" is not supported. You can use one of @ClientTypeEnum.");
        }else if(clientTypes.size() > 1){
            throw new IllegalArgumentException("There exist more than one client type with client code \"" + clientCode + "\".");
        }
        return clientTypes.get(0);
    }

    //valida input file name, existence of the document and that the file can be copied into storage folder
    private static void validateInputFile(File sourceFile, String storageLocation) {
        if(sourceFile.getName().isEmpty()){
            throw new IllegalArgumentException("File name must be filled!");
        }
        if(sourceFile.getName().length() > INPUT_FILE_NAME_LENGTH_LIMIT){
            throw new IllegalArgumentException("File name cannot be longer than "+ INPUT_FILE_NAME_LENGTH_LIMIT + " chars!");
        }
        if(!sourceFile.canRead()){
            throw new IllegalStateException("Source file cannot be read, that file must be saved!");
        }
        String sendingFilePath = parseOutputFileName(sourceFile, storageLocation);
        if(Files.exists(Paths.get(sendingFilePath))){
            throw new IllegalStateException("Source file cannot be copy to \"" + sendingFilePath + "\" because there already exist file with same name!");
        }
    }

    //validate if can be created processing file in storage folder
    private static void validateProcessingFileName(int order, ClientTypeEnum clientType, String storageLocation) {
        if(Files.notExists(Paths.get(storageLocation))){
            throw new IllegalArgumentException("Storage location not exist, use existing location!");
        }
        if(order <= 0){
            throw new IllegalArgumentException("Message order can be only positive number!");
        }
        String fileName = parseProcessingFileName(order, clientType);
        if(Files.exists(Paths.get(storageLocation + "\\" + fileName))){
            throw new IllegalArgumentException("For this input cannot be generated processing file, because there already exist processing file with name " +
                    "\"" + fileName + "\" " + "in folder \"" + storageLocation + "\"");
        }
    }

    //validate if subject and messageBody are valid
    private static void validateMessage(String subject, String messageBody) {
        if(subject == null || subject.isEmpty()){
            throw new IllegalArgumentException("Subject must be filled!");
        }
        if(subject.length() > SUBJECT_LENGTH_LIMIT){
            throw new IllegalArgumentException("Subject cannot be longer than " + SUBJECT_LENGTH_LIMIT + " chars!");
        }
        if(messageBody == null || messageBody.isEmpty()){
            throw new IllegalArgumentException("Message must be filled!");
        }
        if(messageBody.length() > MESSAGE_BODY_LENGTH_LIMIT){
            throw new IllegalArgumentException("Message body cannot be longer than " + MESSAGE_BODY_LENGTH_LIMIT + " chars!");
        }
    }

    //create processing file
    private static File createProcessingFile(String storageLocation, String parseFileName, String outputFileName, String subject, String messageBody) {
        File processingFile = new File(storageLocation + "\\" + parseFileName);
        try{
            processingFile.createNewFile();
            FileWriter writer = new FileWriter(processingFile);
            writer.write("\"" + subject + "\"" + "0" + "\"" + outputFileName + "\",");
            writer.write(System.lineSeparator());
            writer.write(messageBody);
            writer.close();
        }catch (IOException ex){
            throw new RuntimeException("Cannot create processing file: " + ex.toString());
        }
        return processingFile;
    }

    //Create copy of source file
    private static File createOutputFile(File sourceFile, String storageLocation) {
        try{
            Files.copy(sourceFile.toPath(), Paths.get(parseOutputFileName(sourceFile, storageLocation)));
        }catch (IOException ex){
            throw new RuntimeException("Cannot create source file: " + ex.toString());
        }
        return new File(parseOutputFileName(sourceFile, storageLocation));
    }
}
