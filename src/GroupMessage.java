import java.io.File;

/**
 * Transfer object class which have reference to processing file and file which should be attached.
 */
public class GroupMessage {
    private File processingFile;
    private File sendingFile;

    public GroupMessage(File processingFile, File sendingFile) {
        this.processingFile = processingFile;
        this.sendingFile = sendingFile;
    }

    public File getProcessingFile() {
        return processingFile;
    }

    public File getSendingFile() {
        return sendingFile;
    }
}
